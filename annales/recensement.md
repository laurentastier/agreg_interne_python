---
title : 	Sujets de concours contenant des questions en rapport avec Python
geometry : 	margin=2.3cm
---

*Les sujets sont la plupart du temps corrigés.*


# Pour ce qui concerne l'Agrégation Interne

<http://www.agregation-interne-physique-chimie.org/annales-des-eacutepreuves-eacutecrites.html>

## épreuve 2020

### Sujet Physique

Partie 3, pages 15-16 



## épreuve 2021 

### Sujet Physique 

Partie 2, sous-partie C, pages 15-16 du sujet

### Sujet Chimie

Question QP54 page 11 du sujet


-------------------

# Pour ce qui concerne le CAPES

## Épreuve 2021, Étude documentaire

<https://phy-chim.fr/index.php?static3/capes-annales>

page 10, Partie 4 question 31

## Épreuve 2022, sujet 0 (Épreuve écrite disciplinaitre appliquée)

<https://www.devenirenseignant.gouv.fr/cid157873/sujets-zero-2022.html>

page 10, Partie 5, Q22


